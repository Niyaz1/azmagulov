document.addEventListener("DOMContentLoaded", () => {


	// -------animation tablet scroll
	// let tabletPos = 0;
	// let animateTablet = true;

	// $(window).on('scroll', function(){
	//     if( $(window).scrollTop() >= $('.work__tablet-image').offset().top - 300 && animateTablet) {
	//         animateTablet = false;
	//         setInterval(function () {
	//             if ($('.work__tablet-image > .work__image').scrollTop() + $('.work__tablet-image >
	// .work__image').height() <= $('.work__tablet-image > .work__image > .tablet-image').height()) {
	// $('.work__tablet-image > .work__image').animate({scrollTop: tabletPos}, 800); tabletPos += 500; } else { tabletPos
	// = 0; $('.work__tablet-image > .work__image').animate({scrollTop: tabletPos}, 800); } }, 1500); } });

	if (window.outerWidth < 768) {
		$(".work__breadcrumbs").css({"margin-left": "auto", "margin-right": "auto",});
	}

	$('ul.tabs__nav').on('click', '.tabs__item:not(.active)', function () {
		$(this)
				.addClass('active').siblings().removeClass('active')
				.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
	});


	$('#up').click(function () {
		console.log(1);
		$('html, body').animate({scrollTop: 0}, 500);
		return false;
	});

	$(document).on('click', '.contacts-social--icon_email-link', function (event) {
		event.preventDefault();
		let email = 'azfrontendsup@gmail.com';
		let subject = 'Ваша тема';
		let emailBody = 'Здраствуйте.';
		document.location = "mailto:" + email + "?subject=" + subject + "&body=" + emailBody;
	});

	//accordion
	$(".accordion").accordion({
		collapsible: true,
		heightStyle: "content"
	});
	$(".accordion > .accordion__item").blur();
	$('.accordion__item').on('click', (ev) => {
		if ($(window).innerWidth() < 900) {
			let accordionItem = ev.target;
			let accordionOffset = $('.aboutme__left-block-accordion').offset().top;
			$('html, body').animate({scrollTop: accordionOffset - 60});
		}
	});

	//disabling image drag
	$('img').mousedown(() => {
		return false;
	});

	//Active menu header
	let page = location.pathname.replace('/', '');
	const headerMenu = $('.header__menu'),
			footerMenu = $('.footer__menu'),
			headerMobileMenu = $('.header__mobile-menu');

	const regWorksPage = /^w(orks)?(.html)?(\/design)?(\/[0-9]*)?$/,
			regIndexPage = /^i?(index)?(.html)?$/,
			regContactsPage = /^c(ontacts)?(.html)?$/;

	if (page.search(regIndexPage) != -1) {
		headerMenu.children('.header__menu-item[href="/index"]').addClass('header__menu-item--active');
		headerMenu.children('.header__menu-item[href="/index"]').attr('href', '#');
		footerMenu.find('.footer__menu-item[href="/index"]').addClass('footer__menu-item--active');
		footerMenu.find('.footer__menu-item[href="/index"]').attr('href', '#');
		headerMobileMenu.find('.header__mobile-menu-link[href="/index"]').addClass('header__mobile-menu-link--active');
		headerMobileMenu.find('.header__mobile-menu-link[href="/index"]').attr('href', '#');
	}
	if (page.search(regWorksPage) != -1) {
		headerMenu.children('.header__menu-item[href="/works"]').addClass('header__menu-item--active');
		footerMenu.find('.footer__menu-item[href="/works"]').addClass('footer__menu-item--active');
		headerMobileMenu.find('.header__mobile-menu-link[href="/works"]').addClass('header__mobile-menu-link--active');

		if (page === 'works') {
			headerMenu.children('.header__menu-item[href="/works"]').attr('href', '#');
			footerMenu.find('.footer__menu-item[href="/works"]').attr('href', '#');
			headerMobileMenu.find('.header__mobile-menu-link[href="/works"]').attr('href', '#');
		}
	}
	if (page.search(regContactsPage) != -1) {
		headerMenu.children('.header__menu-item[href="/contacts"]').addClass('header__menu-item--active');
		headerMenu.children('.header__menu-item[href="/contacts"]').attr('href', '#');
		footerMenu.find('.footer__menu-item[href="/contacts"]').addClass('footer__menu-item--active');
		footerMenu.find('.footer__menu-item[href="/contacts"]').attr('href', '#');
		headerMobileMenu.find('.header__mobile-menu-link[href="/contacts"]').addClass('header__mobile-menu-link--active');
		headerMobileMenu.find('.header__mobile-menu-link[href="/contacts"]').attr('href', '#');
	}

	// popup close
	$(document).on('click', el => {
		let btnClass = el.target;
		let popupCloseIcon = $(btnClass).hasClass('popup__icon-close');

		if (popupCloseIcon) {
			bodyScrollLock.clearAllBodyScrollLocks($('html,body'));
			$('.design-site__brief').removeClass('popup--show');
			$('.error-navigate').removeClass('error-navigate--show');
			$('.popup__preloader').removeClass('popup__preloader--show');
			$('.popup__logotype-AZ').removeClass('popup__logotype-AZ--show');
		}
		if ($('.design-site__brief').hasClass('popup--show') && !$(btnClass).parentsUntil('.aboutme').hasClass('developing')) {
			bodyScrollLock.clearAllBodyScrollLocks($('html,body'));
			$('.design-site__brief').removeClass('popup--show');
			$('.error-navigate').removeClass('error-navigate--show');
			$('.popup__preloader').removeClass('popup__preloader--show');
			$('.popup__logotype-AZ').removeClass('popup__logotype-AZ--show');
		}
	});

	//fixed menu
	// let heightMainWindow;
	// $(window).bind('scroll', () => {
	//
	//   heightMainWindow = $('.main-window').height() - 87;
	//
	//   if ($(window).scrollTop() > 100 && $(window).innerWidth() < 576) {
	//     $('.header__mobile-menu').addClass('header__mobile-menu--fixed');
	//   } else {
	//     $('.header__mobile-menu').removeClass('header__mobile-menu--fixed');
	//   }
	//
	//   if ($(window).scrollTop() > heightMainWindow && $(window).innerWidth() > 576 && $('.header__menu').length < 2) {
	//     $('.header__menu').clone().prependTo('.header').addClass('header__menu--fixed');
	//   }
	//
	//   if ($(window).scrollTop() > heightMainWindow && $(window).innerWidth() > 576 && $('.header__menu--fixed')) {
	//     $('.header__menu--fixed').slideDown(100);
	//   }
	//
	//   if ($(window).scrollTop() < heightMainWindow && $(window).innerWidth() > 576 && $('.header__menu').length > 1) {
	//     $('.header__menu--fixed').slideUp(100);
	//   }
	// });

	//Slider
	function slider(blockName) {

		let SLIDER = blockName;
		let slides;
		let slidesArray;
		let arrowLeft;
		let arrowRight;
		let dotsContainer;
		let dots;
		let dotsArray;
		let activeSlide;

		const setSlider = function () {
			if ($(SLIDER).length) {
				console.log(SLIDER);
				slides = $(SLIDER).find('.slider__item');
				slidesArray = [...slides];
				arrowLeft = $(SLIDER).find('.icon-arrow-left');
				arrowRight = $(SLIDER).find('.icon-arrow-right');
				dotsContainer = $(SLIDER).find('.slider__dot-navigation');

				if (slidesArray.length > 1) {
					for (let i = 0; i < slidesArray.length; i++) {
						if (i == 0)
							$(dotsContainer).append('<span class="slider__dot slider__dot--active"></span>');
						else
							$(dotsContainer).append('<span class="slider__dot"></span>');
					}
				} else {
					arrowLeft.css('display','none');
					arrowRight.css('display','none');
				}

				dots = $(SLIDER).find('.slider__dot-navigation').children();
				dotsArray = [...dots];

				//RightArrow click
				arrowRight.click(function () {
					activeSlide = $(SLIDER).find('.slider__item--active');
					activeSlide.next().addClass('slider__item--active slider__item--animate-to-right').removeClass('slider__item--animate-to-left').prev().removeClass('slider__item--active');

					$(dotsArray[$('.slider__item--active').index()]).addClass('slider__dot--active');
					$(dotsArray[$('.slider__item--active').prev().index()]).removeClass('slider__dot--active');

					if ($('.slider__item--active').next().index() == slides.length) {
						$(arrowRight).addClass('icon-hide').removeClass('icon-show');
					}
					if ($('.slider__item--active').index() > 0) {
						$(arrowLeft).addClass('icon-show').removeClass('icon-hide');
					}
				});

				//LeftArrow click
				arrowLeft.click(function () {
					activeSlide = $(SLIDER).find('.slider__item--active');
					activeSlide.prev().addClass('slider__item--active').next().removeClass('slider__item--active slider__item--animate-to-right').addClass('slider__item--animate-to-left');

					$(dotsArray[$('.slider__item--active').index()]).addClass('slider__dot--active');
					$(dotsArray[$('.slider__item--active').next().index()]).removeClass('slider__dot--active');

					if ($('.slider__item--active').next().index() == slides.length - 1) {
						$(arrowRight).addClass('icon-show').removeClass('icon-hide');
					}
					if ($('.slider__item--active').index() == 0) {
						$(arrowLeft).addClass('icon-hide').removeClass('icon-show');
					}
				});

				$(slides).hover(function () {
					if (!$(this).hasClass('slider__item--active')) {

						$(this).toggleClass('slider__item--hover-next-slide');

						$(this).click(function () {
							$(this).addClass('slider__item--active').removeClass('slider__item--animate-to-left').addClass('slider__item--animate-to-right');
							$(this).prev().removeClass('slider__item--active');
							$(this).removeClass('slider__item--hover-next-slide');
							$(dotsArray[$(this).index()]).addClass('slider__dot--active');
							$(dotsArray[$(this).prev().index()]).removeClass('slider__dot--active');

							if ($('.slider__item--active').next().index() == slides.length) {
								$(arrowRight).addClass('icon-hide').removeClass('icon-show');
							}

							if ($('.slider__item--active').index() > 0) {
								$(arrowLeft).addClass('icon-show').removeClass('icon-hide');
							}
						})
					}
				});

				// $('.slider__dot-navigation span').click(function() {
				//
				//     $(dotsArray).removeClass('slider__dot--active');
				//
				//     $(this).toggleClass('slider__dot--active');
				//
				//     console.log(indexActiveSlide);
				//
				//     $(slides).removeClass('slider__item--active');
				//     $(slide).addClass('slider__item--active');
				//
				//     $(slide).addClass('slider__item--animate-to-right');
				//
				// });

			}

		};

		setSlider();

	}

	slider('.slider');
	slider('.slider-develop');

});
