//page-preloader
const page = document.getElementsByTagName('body');
page[0].classList.add('scroll-stop');

const images = document.images;
const imagesTotalCount = images.length;
let imagesLoadedCount = 0;
// const percDisplay = document.getElementsByClassName('load-perc');
const preloader = document.getElementsByClassName('page-preloader');
let preloadDone = preloader[0].classList.contains('page-preloader--done');

for (let i = 0; i < images.length; i++) {
    let imageClone = new Image();
    imageClone.onload = imageLoaded;
    imageClone.onerror = imageLoaded;
    imageClone.src = images[i].src;
}

function imageLoaded() {
    imagesLoadedCount++;
    // percDisplay.innerHTML = (((100 / imagesTotalCount) * imagesLoadedCount ) << 0 ) + '%';

    if (imagesLoadedCount >= imagesTotalCount) {
        setTimeout(() => {
            if ( !preloadDone ) {
                preloader[0].classList.add('page-preloader--done');
                page[0].classList.remove('preloader--scroll-stop');
            }
        }, 600)
    }
}