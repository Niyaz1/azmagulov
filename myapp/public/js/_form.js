document.addEventListener("DOMContentLoaded", () => {
	const successClass = 'form__textarea--success';
	const errorClassCheckboxWithTextarea = 'form__textarea--checked-textarea-error error-shake';
	const errorClass = 'form--error error-shake';
	const errorClassSwitchbox = 'error-shake-switchbox';

	//get out LocalStorage when click button which the open form and download form with AJAX
	$('.design-site__brief-button').on('click', ev => {
		ev.preventDefault();
		animatePopup();
		// uploadBriefFormAJAX();
		uploadFormMessage();
	});

	$('.developing-site-order__button').on('click', ev => {
    ev.preventDefault();
    animatePopup();
    uploadFormMessageDeveloping();
  });

	saveToLocalStorage();

	//click send button form
	$(document).on('click', el => {
		let designBriefFormButton = $(el.target).parent().hasClass('design-site__form-button');
		let closeSuccessMessageButton = $(el.target).parent().hasClass('popup__success-icon-close');
		let contactsButton = $(el.target).parent().hasClass('popup__contacts-button');
		const contactFormClose = $(el.target).parent().hasClass('contacts-form__icon-close');
		let designOrderForm = $(el.target).parent().hasClass('design-order-form__button');

		if (designOrderForm) {
			el.preventDefault();
			let preloader = $('.popup__preloader');
			let logotype = $('.popup__logotype-AZ');
			let form = $('form[name=design-order-form]');
			let errors = errorChecking();

			if (errors.length > 0) {
				errorCorrection(errors);
			} else {
				const method = form.attr('method');
				const url = form.attr('action');
				let dataToSend = dataCollection();

				$.ajax({
					type: method,
					url: url,
					data: dataToSend,
					beforeSend: () => {
						$('.popup').animate({scrollTop: 0}, 300).addClass('popup--scroll-stop');
						preloader.addClass('popup__preloader--show');
						logotype.addClass('popup__logotype-AZ--show');
					},
					success: msg => {
						$('.popup__textarea').val('');
						$('.contacts-form__input-text').val('');
						localStorage.removeItem('client-email');
						localStorage.removeItem('client-message');

						setTimeout(ev => {
							if (preloader.hasClass('popup__preloader--show')) {
								$('.popup__success__client-email').text(dataToSend.email);

								$('.popup__success').addClass('popup__success--show');
								$('.popup__content').html(msg);
								preloader.removeClass('popup__preloader--show');
								logotype.removeClass('popup__logotype-AZ--show');
								$('.popup').removeClass('popup--scroll-stop');
							}
						}, 2000);
					},
					complete: msg => {
						// preloader.removeClass('popup__preloader--show');
						// logotype.removeClass('popup__logotype-AZ--show');
						// $('.popup').removeClass('popup--scroll-stop');
					}
				});
			}

		}

		if (contactFormClose) {
			$('.popup__contacts-form').fadeOut(200);
		}

		if (contactsButton) {
			el.preventDefault();
			$('.popup__contacts-form').fadeIn(200);
		}

		if (designBriefFormButton) {
			el.preventDefault();
			let preloader = $('.popup__preloader');
			let logotype = $('.popup__logotype-AZ');
			let form = $('form[name=brief-design-site]');
			let errors = errorChecking();

			if (errors.length > 0) {
				errorCorrection(errors);
			} else {
				const method = form.attr('method');
				const url = form.attr('action');
				let dataToSend = dataCollection();

				$.ajax({
					type: method,
					url: url,
					data: dataToSend,
					beforeSend: () => {
						$('.popup').animate({scrollTop: 0}, 300).addClass('popup--scroll-stop');
						preloader.addClass('popup__preloader--show');
						logotype.addClass('popup__logotype-AZ--show');
					},
					success: msg => {
						setTimeout(ev => {
							if (preloader.hasClass('popup__preloader--show')) {
								$('.popup__success__client-email').text(dataToSend.email);
								$('.popup__success').addClass('popup__success--show');
								$('.popup__content').html(msg);
								preloader.removeClass('popup__preloader--show');
								logotype.removeClass('popup__logotype-AZ--show');
								$('.popup').removeClass('popup--scroll-stop');
							}
						}, 2000);
					},
					complete: msg => {
						// preloader.removeClass('popup__preloader--show');
						// logotype.removeClass('popup__logotype-AZ--show');
						// $('.popup').removeClass('popup--scroll-stop');
					}
				});
			}
		}

		if (closeSuccessMessageButton) {
			$('.popup__success').removeClass('popup__success--show');
		}
	});

	//show textarea when radio checked
	$(document).on('click', '.form__radio', el => {
		let radioInput = $(el.target).hasClass('form__radio-input');
		let radioLabel = $(el.target).hasClass('form__radio-label');
		let radioInsideTextarea = $(el.target).parent().parent().hasClass('form__radio--checked-textarea');
		let textareaInsideRadio = false;
		let radioLabelElement;
		let radioInputElement;
		let radioCheckedTextarea = $(el.target).hasClass('form__textarea--radio-checked');

		if (radioLabel) {
			radioLabelElement = $(el.target);
			radioInputElement = $(el.target).siblings('.form__radio-input');
		}

		if (radioInput) {
			radioInputElement = $(el.target);
			radioLabelElement = $(el.target).siblings('.form__radio-label');
		}

		if (radioInsideTextarea) {
			if (radioInputElement.prop('checked')) {
				textareaInsideRadio = $(radioLabelElement).children('.form__textarea--radio-checked');
				$(textareaInsideRadio).slideDown(200);
			} else {
				$(textareaInsideRadio).slideUp(200);
			}
		}

		if (!radioInsideTextarea && !textareaInsideRadio && !radioCheckedTextarea) {
			$('.form__textarea--radio-checked').slideUp(200);
		}
	});

	//show textarea when checkbox checked
	$(document).on('click', '.form__checkbox--checked-textarea', el => {
		let checkboxLabelInsideTextarea = $(el.target).parent().parent().hasClass('form__checkbox--checked-textarea');
		let checkboxInput = $(el.target).hasClass('form__checkbox-input');
		let checkboxLabel = $(el.target).hasClass('form__radio-label');
		let checkboxInputElement;
		let checkboxLabelElement;

		if (checkboxLabel) {
			checkboxLabelElement = $(el.target);
			checkboxInputElement = $(el.target).siblings('.form__checkbox-input');
		}

		if (checkboxInput) {
			checkboxInputElement = $(el.target);
			checkboxLabelElement = $(el.target).siblings('.form__checkbox-label');
		}

		if (checkboxLabelInsideTextarea) {
			if ($(checkboxInputElement).prop('checked')) {
				$(checkboxLabelElement).children('.form__textarea--checkbox-checked').slideDown(200);
			} else {
				$(checkboxLabelElement).children('.form__textarea--checkbox-checked').slideUp(200);
			}
		}
	});

	//file-input
	$(document).on('change', '.popup__form-label-file > .form__file-input', el => {
		// const form = $('form[name=brief-design-site]');
		// const method = form.attr('method');
		// const url = '/i/finished-design-brief/image';
		const fileTypes = ['image/jpeg', 'image/pjpeg', 'image/png'];
		let fileName = '';
		let files = el.target.files;
		let labelFileText = $(el.target).siblings('.form__file-input-label');
		// let filesd = new FormData();
		let errorFiles = [];

		if (files && files.length > 1) {
			fileName = (el.target.getAttribute('data-multiple-caption') || '').replace('{count}', files.length);
		} else {
			fileName = el.target.value.split('\\').pop();
		}

		if (isNaN(fileName)) {

			$.each(files, (key, value) => {
				let valid = validFileType(value);
				let size = returnFileSize(value.size);

				if (size) {
					if (valid) {
						// filesd.append(key, value);
						labelFileText.html(fileName);
					} else {
						errorFiles.push('Запрещенный тип файла!');
						console.error('Unsupported file type!');
					}
				} else {
					errorFiles.push('Файл больше 2-х Мб!');
					console.error('File larger than 2 Mb!');
				}
			});

			if (errorFiles.length > 0) {
				labelFileText.addClass('form__file-input-label--error');
				let errorString = '';

				errorFiles.forEach((element, index) => {
					errorString += element + ', ';
				});
				labelFileText.parent().append('<span class="form__file-error-message">' + errorString + '</span>');

			} else {
				labelFileText.removeClass('form__file-input-label--error');
				let errorMessage = '';
				// labelFileText.siblings('.form__file-error-message').html('');
				// filesd.append('file_update-logotype', 1);

				// $.ajax({
				//   url: url,
				//   type: method.toUpperCase(),
				//   data: filesd,
				//   cache: false,
				//   dataType: 'json',
				//   processData: false,
				//   contentType: false,
				//   success: (respond, status, jqXHR) => {
				//     if (typeof respond.error === 'undefined') {
				//       console.log(respond);
				//       let filesPath = respond.files;
				//       let html = '';
				//       $.each(filesPath, (key, value) => {
				//         html += value + '<br>';
				//       });
				//       console.log(html);
				//     } else {
				//       console.log('Error! - ' + respond.error);
				//     }
				//     $(el.target).siblings('.form__file-input-label').text(fileName);
				//   },
				//   error: (jqXHR, status, errorThrown) => {
				//     console.log('error AJAX request - ' + status, jqXHR);
				//   }
				// });
			}

			function validFileType(file) {
				for (let i = 0; i < fileTypes.length; i++) {
					if (files[i].type === fileTypes[i]) {
						return true;
					}
				}
				return false;
			}

			function returnFileSize(number) {
				if (number < 2048576) {
					if (number < 1024) {
						return number + 'bytes';
					} else if (number > 1024 && number < 1048576) {
						return (number / 1024).toFixed(1) + 'KB';
					} else if (number > 1048576) {
						return (number / 1048576).toFixed(1) + 'MB';
					}
				} else {
					return false;
				}
			}
		}
	});

	// function preloaderSmile() {
	//   const letters = ['T', 'U', 'O', 'D', 'C', 'S', 'P', 'Y', 'X'];
	//   const preloader = document.querySelector('.preloader-face');
	//   let previousIndex = null;
	//   let prePreviousIndex = null;
	//   const getRandomIndex = () => Math.round((letters.length - 1) * Math.random());
	//   const preloaderChange = () => {
	//     let randomIndex = getRandomIndex();
	//     while (randomIndex === previousIndex ||
	//       randomIndex === prePreviousIndex) {
	//       randomIndex = getRandomIndex();
	//     }
	//     prePreviousIndex = previousIndex;
	//     previousIndex = randomIndex;
	//     const l = letters[randomIndex];
	//     preloader.innerHTML = l + '_' + l
	//  }
	//  setInterval(preloaderChange, 150);
	// }
	function uploadBriefFormAJAX() {
		$.ajax({
			type: "POST",
			url: '/i/popup-brief-design-site',
			data: {data: 1},
			success: msg => {
				$('.design-site__brief').html(msg);
				fillingOutLocalStorage();

				// drag&drop form
				$('.form__checkbox-list').sortable({
					items: "> .form__checkbox--list-item",
					placeholder: "ui-state-highlight",
					forceHelperSize: true,
					forcePlaceholderSize: true,
					cursor: 'move',
				});
				$('.form__checkbox-list').disableSelection();
			},
			error: () => {
				console.error('Ошибка запроса! Повторите попытку.');
			}
		});
	}

	function uploadFormMessage() {
		$.ajax({
			type: "POST",
			url: '/i/design-order',
			data: {data: 1},
			success: msg => {
				$('.design-site__brief').html(msg);
				fillingOutLocalStorage();
			},
			error: () => {
				console.error('Ошибка запроса! Повторите попытку.');
			}
		});
	}

  function uploadFormMessageDeveloping() {
    $.ajax({
      type: "POST",
      url: '/i/developing-order',
      data: {data: 1},
      success: msg => {
        $('.design-site__brief').html(msg);
        fillingOutLocalStorage();
      },
      error: () => {
        console.error('Ошибка запроса! Повторите попытку.');
      }
    });
  }

	function animatePopup() {
		bodyScrollLock.disableBodyScroll($('html,body'), {
			allowTouchMove: el => {
				while (el && el !== document.body) {
					if (el.getAttribute('body-scroll-lock-ignore') !== null) {
						return true
					}
					el = el.parentNode
				}
			},
		});
		$('.design-site__brief').offset({top: document.documentElement.scrollTop + 20});
		$('.design-site__brief').height($(window).height() - 120);
		$('.design-site__brief').addClass('popup--show');
	}

	function dataCollection() {
		let data = {};
		data.textarea = [];
		data.inputText = [];
		data.checkbox = [];
		data.radio = [];
		data.email = $('input[type=email]').val().trim();

		$('.error-navigate').removeClass('error-navigate--show');

		$('textarea').each(function (index, element) {
			if ($(element).val().trim() != '') {
				let textareaInsideCheckbox = $(element).hasClass('form__textarea--checkbox-checked');
				let textareaInsideRadio = $(element).hasClass('form__textarea--radio-checked');
				let textareaObj = {};

				if (!textareaInsideCheckbox && !textareaInsideRadio) {
					textareaObj.textLabel = $(element).siblings('.form__label-text').text();
					textareaObj.val = $(element).val();
					data.textarea.push(textareaObj);
				}
			}
		});

		$('input[type=text]').each(function (index, element) {
			if ($(element).val().trim() != '') {
				let inputText = $(element).val().trim();
				let labelText = $(element).siblings('.form__label-text').text();
				let inputTextObj = {};

				inputTextObj.label = labelText;
				inputTextObj.text = inputText;
				data.inputText.push(inputTextObj);
			}
		});

		$('input[type=radio]:checked').each(function (index, element) {
			let radioLabel = $(element).siblings('.form__radio-label').text().trim();
			let radioList = $(element).parentsUntil('.form');
			let radioListLabel = $(radioList[radioList.length - 1]).children('.form__label-text').text().trim();
			let radioInsideTextarea = $(element).parent().parent().hasClass('form__radio--checked-textarea');
			let radioObj = {};
			let radioTextareaText;

			radioObj.listLabel = radioListLabel;
			if (radioInsideTextarea) {
				radioTextareaText = $(element).siblings('.form__radio-label').children('.form__textarea--radio-checked').val().trim();
				radioObj.labelTextarea = radioLabel;
				radioObj.textarea = radioTextareaText;
			}
			if (!radioInsideTextarea) {
				radioObj.label = radioLabel;
			}

			data.radio.push(radioObj);
		});

		$('input[type=checkbox]:checked').each(function (index, element) {
			let checkboxLabelText = $(element).siblings('.form__checkbox-label').text().trim();
			let checkboxList = $(element).parentsUntil('.form');
			let checkboxListLabel = $(checkboxList[checkboxList.length - 1]).children('.form__label-text').text().trim();
			let checkboxInsideTextarea = $(element).parent().parent().hasClass('form__checkbox--checked-textarea');
			let checkboxTextareaText;
			let checkboxObj = {};
			let checkboxSortable = $(element).parent().parent().hasClass('form__checkbox--drag-drop');
			let indexSortable;

			checkboxObj.listLabel = checkboxListLabel;
			checkboxObj.textLabel = checkboxLabelText;

			if (checkboxInsideTextarea) {
				checkboxTextareaText = $(element).siblings('.form__checkbox-label').children('.form__textarea--checkbox-checked').val().trim();
				checkboxObj.textarea = checkboxTextareaText;
			}

			if (checkboxSortable) {
				indexSortable = $(element).parent().parent().index();
				checkboxObj.sortable = true;
				checkboxObj.indexSortable = indexSortable;
			}

			data.checkbox.push(checkboxObj);
		});

		return data;
	}

	function errorChecking() {
		let errors = [];
		let listCheckboxs = $('.form__item-list');
		let textareas = $('.form__textarea:required');
		let inputs = $('.form__input-text:required');
		let switchboxs = $('.form__switchbox-input:required');
		let parentForm; // child form
		let formHeight = $('.popup__form').height() - 300;

		// Checked requires list checkboxes
		$(listCheckboxs).each(function (index, element) {
			let requireList = $(element).children('.form__label-text').hasClass('form__label-text--required');
			let checkboxList = $(element).children().hasClass('form__checkbox-list');
			let checkbox = {};
			let list;

			if (requireList && checkboxList) {
				let checkedInput = $(element).find('.form__checkbox-input:checked');
				// if no item checked --> save Array errors
				if (checkedInput.length < 1) {
					list = checkedInput.prevObject;
					checkbox.element = list;
					checkbox.positionElement = formHeight + list.position().top;
					errors.push(checkbox);
				}
			}
		});

		// Checked requires textareas
		$(textareas).each(function (index, element) {
			let value = $(element).val().trim();
			let textareaInsideCheckbox = $(element).hasClass('form__textarea--checkbox-checked');
			let textareaInsideRadio = $(element).hasClass('form__textarea--radio-checked');
			let inputChecked; // если textarea находиться внутри чекбокса
			let positionParentError; // позиция этого дочернего элемента
			let textarea = {};
			let parentElement;
			let parentElementPosition;
			let formHeight = $('.popup__form').height() - 300;
			let checkboxDragAndDrop = $(element).parent().parent().parent().hasClass('form__checkbox--drag-drop');

			// if textarea inside checkbox or radio
			if (textareaInsideCheckbox || textareaInsideRadio) {
				// find checked checkbox or radio
				inputChecked = $(element).parent().parent().children('input').prop('checked');
				// if checkbox or radio checked and textarea value = 0
				if (inputChecked && value.length < 1) {
					// list inside form
					if (checkboxDragAndDrop) {
						parentElement = $(element).parentsUntil('.form__item-list');
					} else {
						parentElement = $(element).parentsUntil('.form__checkbox-list');
					}
					// position list inside form
					parentElementPosition = formHeight + $(parentElement[parentElement.length - 1]).position().top;
					//create error object
					textarea.element = $(element);
					textarea.positionElement = parentElementPosition;
					// save in Array errors
					errors.push(textarea);
				}
			}

			// other textarea
			if (value.length < 1 && !textareaInsideCheckbox && !textareaInsideRadio) {
				parentForm = $(element).parentsUntil('.form');
				positionParentError = formHeight + $(parentForm[parentForm.length - 1]).position().top;
				textarea.element = $(element);
				textarea.positionElement = positionParentError;
				errors.push(textarea);
			}
		});

		// Checked input-text required
		$(inputs).each(function (index, element) {
			let value = $(element).val().trim();
			let inputs = {};
			let formHeight = $('.popup__form').height() - 300;

			if (value.length < 1) {
				parentForm = $(element).parentsUntil('.form');
				positionParentError = formHeight + $(parentForm[parentForm.length - 1]).position().top;
				inputs.element = $(element);
				inputs.positionElement = positionParentError;
				errors.push(inputs);
			}
		});

		// Checked switchboxs required
		$(switchboxs).each(function (index, element) {
			let switchbox = {};
			let switchboxChecked = $(element).prop('checked');
			let formHeight = $('.popup__form').height() - 300;

			if (!switchboxChecked) {
				parentForm = $(element).parentsUntil('.form');
				positionParentError = formHeight + $(parentForm[parentForm.length - 1]).position().top;
				switchbox.element = $(element);
				switchbox.positionElement = positionParentError;
				switchbox.errorText = "Дайте согласие на обработку ваших данных.";
				errors.push(switchbox);
			}
		});

		// Sort errors Array
		errors.sort(function (a, b) {
			if (a.positionElement > b.positionElement) {
				return 1;
			}
			if (a.positionElement < b.positionElement) {
				return -1;
			}
			return 0;
		});
		return errors;
	}

	function errorCorrection(errors) {
		let parentElement;
		let numberError = 0;
		let errorElement = $(errors[numberError].element);
		let errorTextElement = errors[numberError].errorText;
		let errorTextareaInsideCheckbox = errorElement.hasClass('form__textarea--checkbox-checked');
		let errorSwithcbox = errorElement.hasClass('form__switchbox-input');

		// if error inside checkbox
		if (errorTextareaInsideCheckbox && !errorSwithcbox) {
			parentElement = errorElement.parent().parent().parent();
			parentElement.addClass(errorClassCheckboxWithTextarea);
		} else {
			errorElement.addClass(errorClass);
		}

		// if error inside switchbox
		if (errorSwithcbox) {
			errorElement.siblings('.form__switchbox-label').addClass(errorClassSwitchbox);
			$('.error-navigate').find('.error-navigate__message').html(errorTextElement);
			$('.error-navigate').offset({top: document.documentElement.scrollTop + 20}).addClass('error-navigate--show');
		} else {
			$('.error-navigate').offset({top: document.documentElement.scrollTop + 20}).addClass('error-navigate--show');
		}

		$('.popup').animate({scrollTop: errors[numberError].positionElement}, 800);
	}

	function fillingOutLocalStorage() {
		if (localStorage.length > 0) {
			let keys = [];
			for (let i = 0; i < localStorage.length; i++) {
				keys[i] = localStorage.key(i);
				let value = localStorage.getItem(keys[i]);
				let domElem = $('.design-site__brief').find("[name='" + keys[i] + "']");
				let requiredElements = $(domElem).siblings('.form__label-text').hasClass('form__label-text--required');

				let inputElement = $(domElem).attr('type') === 'text';
				let emailElement = $(domElem).attr('type') === 'email';
				let textareaElement = $(domElem).hasClass('form__textarea');
				let checkboxElement = $(domElem).attr('type') === 'checkbox';
				let checkboxElementWithTextarea = $(domElem).parent().parent().hasClass('form__checkbox--checked-textarea');
				let checkboxTextarea = $(domElem).siblings('.form__checkbox-label').children('.form__textarea--checkbox-checked');
				let radioElement = $(domElem).attr('type') === 'radio';
				let radioElementWithTextarea;
				let radioElementTextarea;

				if (inputElement || textareaElement || emailElement) {
					$(domElem).val(value);
					if (requiredElements) {
						$(domElem).addClass(successClass);
					}
				}

				if (checkboxElement) {
					$(domElem).prop('checked', true);

					if (checkboxElementWithTextarea) {
						checkboxTextarea.slideDown(200);
					}
				}

				if (radioElement) {
					$(domElem).each(function () {
						radioElementWithTextarea = $(this).parent().parent().hasClass('form__radio--checked-textarea');
						radioElementTextarea = $(this).siblings('.form__radio-label').children('.form__textarea--radio-checked');

						if ($(this).val() === value) {
							$(this).prop('checked', true);

							if (radioElementWithTextarea) {
								radioElementTextarea.slideDown(200);
							}
						}
					});
				}
			}
		}
	}

	function saveToLocalStorage() {
		$(document).on('blur', '.form__label > textarea', el => {
			let textarea = $(el.target);
			let nameTextarea = textarea.attr('name');
			let parentElement;
			let textareaValue = textarea.val().trim();
			let textareaInsideCheckbox = textarea.hasClass('form__textarea--checkbox-checked');
			let textareaInsideRadio = textarea.hasClass('form__textarea--radio-checked');
			let containerTextareaInsideCheckboxOrRadio = textarea.parent().parent().parent();

			if (textareaValue.length > 0) {
				if (el.target.required) {
					if (textareaInsideCheckbox || textareaInsideRadio) {
						containerTextareaInsideCheckboxOrRadio.addClass(successClass).removeClass(errorClassCheckboxWithTextarea);
					} else {
						textarea.addClass(successClass).removeClass(errorClass);
					}
				}
				localStorage.setItem(nameTextarea, textareaValue);

			} else {
				if (el.target.required) {
					if (textareaInsideCheckbox || textareaInsideRadio) {
						parentElement = textarea.parent().parent().parent();
						parentElement.addClass(errorClassCheckboxWithTextarea).removeClass(successClass);
					} else {
						textarea.addClass(errorClass).removeClass(successClass);
					}
				}
				localStorage.removeItem(nameTextarea);
			}
		});

		$(document).on('blur', '.form__label > input', el => {
			let input = $(el.target);
			let nameInput = input.attr('name');
			let inputTextValue = input.val().trim();

			if (inputTextValue.length > 0) {
				if (el.target.required) {
					input.addClass(successClass).removeClass(errorClass);
				}
				localStorage.setItem(nameInput, inputTextValue);
			} else {
				if (el.target.required) {
					input.addClass(errorClass).removeClass(successClass);
				}
				localStorage.removeItem(nameInput);
			}
		});

		$(document).on('change', '.form__checkbox-container > input', el => {
			let checkbox = $(el.target);
			let nameCheckbox = checkbox.attr('name');
			let checkboxValue = checkbox.val();
			let checkboxContainer = checkbox.parent().parent();
			let checkboxsParents = checkbox.parentsUntil('.form');
			let checkboxChecked = checkbox.prop('checked');

			let checkboxList = checkboxsParents[checkboxsParents.length - 1];
			let checkboxListError = $(checkboxList).hasClass(errorClass);
			let checboxListRequired = $(checkboxList).children('.form__label-text').hasClass('form__label-text--required');

			let checkCheckboxInsideTextarea = checkboxContainer.hasClass('form__checkbox--checked-textarea');

			if (checkboxChecked) {
				if (checboxListRequired && checkboxListError) {
					$(checkboxList).removeClass(errorClass);
				}
			} else {
				if (checkCheckboxInsideTextarea) {
					checkboxContainer.removeClass(errorClassCheckboxWithTextarea);
				}
			}

			if (localStorage.getItem(nameCheckbox) === null) {
				localStorage.setItem(nameCheckbox, checkboxValue);
			} else {
				localStorage.removeItem(nameCheckbox);
			}
		});

		$(document).on('change', '.form__radio-container > input', el => {
			let radio = $(el.target);
			let nameRadio = radio.attr('name');
			let checkedRadio = radio.prop('checked');
			let radioContainer = radio.parent().parent();
			let radioContainerInsideTextarea = radioContainer.hasClass('form__radio--checked-textarea');

			if (checkedRadio && !radioContainerInsideTextarea) {
				$('.form__radio').removeClass(errorClassCheckboxWithTextarea);
			}

			localStorage.setItem(nameRadio, radio.val());
		});
	}

});
