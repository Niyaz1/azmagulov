var gulp         = require('gulp'),
		sass         = require('gulp-sass'),
		browserSync  = require('browser-sync'),
		concat       = require('gulp-concat'),
		uglify       = require('gulp-uglify-es').default,
		cleancss     = require('gulp-clean-css'),
		autoprefixer = require('gulp-autoprefixer'),
		fileinclude  = require('gulp-file-include'),
		rsync        = require('gulp-rsync'),
		newer        = require('gulp-newer'),
		babel        = require('gulp-babel'),
		rename       = require('gulp-rename'),
		responsive   = require('gulp-responsive'),
    del          = require('del');

// Local Server
gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: '/'
		},
		notify: false,
    // proxy: 'http://localhost:3000'
		// tunnel: 'projectname'
		// online: false, // Work offline without internet connection
		// tunnel: true, tunnel: 'projectname', // Demonstration page: http://projectname.localtunnel.me
	})
});
function bsReload(done) { browserSync.reload(); done(); };

// Gulp-Live-Server
// gulp.task('serve', function() {
//   var serve = gls.static();
//   server.start();
// });

// gulp.task('browser-sync', function () {
// 	browserSync.init({        // files to inject
// 		proxy: "http://portfoliio",
// 		port: 3000
// 	});
// });
// function bsReload(done) { browserSync.reload(); done(); };

// gulp.task('php', function () {
// 	return gulp.src('app/*.php')
// 	.pipe(browserSync.reload({ stream: true }))
// });

// html include with PostHTML
// gulp.task('fileinclude-index', function () {
// 	return gulp.src([
// 		'app/components/aboutme/about.html'
// 		])
// 		.pipe(fileinclude({
// 			prefix: '@@',
// 			basepath: 'app/components/'
// 		}))
// 		.pipe(rename('./index.html'))
// 		.pipe(gulp.dest('app/'))
// 		.pipe(browserSync.reload({ stream: true }));
// });

// gulp.task('fileinclude-contacts', function () {
// 	return gulp.src([
// 		'app/components/contacts/contacts.html'
// 	])
// 		.pipe(fileinclude({
// 			prefix: '@@',
// 			basepath: 'app/components/'
// 		}))
// 		.pipe(gulp.dest('app/'))
// 		.pipe(browserSync.reload({ stream: true }));
// });

// gulp.task('fileinclude-works', function () {
// 	return gulp.src([
// 		'app/components/works/works.html'
// 	])
// 		.pipe(fileinclude({
// 			prefix: '@@',
// 			basepath: 'app/components/'
// 		}))
// 		.pipe(gulp.dest('app/'))
// 		.pipe(browserSync.reload({ stream: true }));
// });

// gulp.task('preloader-styles', function() {
// 	return gulp.src('app/sass/preloader.sass')
// 	.pipe(sass({ outputStyle: 'expanded' }))
// 	.pipe(autoprefixer({
// 		grid: true,
// 		overrideBrowserslist: ['last 10 versions']
// 	}))
// 	.pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Optional. Comment out when debugging
// 	.pipe(gulp.dest('app/css'))
// 	.pipe(browserSync.stream())
// });

// Custom Styles
gulp.task('styles', function() {
	return gulp.src('./sass/**/*.sass')
	.pipe(sass({ outputStyle: 'expanded' }))
	.pipe(concat('styles.min.css'))
	.pipe(autoprefixer({
		grid: true,
		overrideBrowserslist: ['last 10 versions']
	}))
	.pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Optional. Comment out when debugging
	.pipe(gulp.dest('./css'))
	.pipe(browserSync.stream())
});


gulp.task('preloader-script', function() {
	return gulp.src('./js/preloader.js')
	.pipe(babel())
	.pipe(uglify()) // Minify js (opt.)
	.pipe(rename('./preloader.min.js'))
	.pipe(gulp.dest('./js'))
	.pipe(browserSync.stream())
});

// Scripts & JS Libraries
gulp.task('scripts', function() {
	return gulp.src([
		'node_modules/jquery/dist/jquery.min.js', // Optional jQuery plug-in (npm i --save-dev jquery)
		'node_modules/jquery-ui-dist/jquery-ui.min.js',
		'node_modules/body-scroll-lock/lib/bodyScrollLock.min.js',
		'./js/_lazy.js', // JS library plug-in example
		'./js/_form.js',
		'./js/_custom.js' // Custom scripts. Always at the end
		])
	.pipe(babel())
	.pipe(concat('scripts.min.js'))
	.pipe(uglify()) // Minify js (opt.)
	.pipe(gulp.dest('./js'))
	.pipe(browserSync.reload({ stream: true }))
});

// Responsive Images
// var quality = 95; // Responsive images quality

// // Produce @1x images
// gulp.task('img-responsive-1x', async function() {
// 	return gulp.src('app/img/_src/**/*.{png,jpg,jpeg,webp,raw}')
// 		.pipe(newer('app/img/@1x'))
// 		.pipe(responsive({
// 			'**/*': { width: '50%', quality: quality }
// 		})).on('error', function (e) { console.log(e) })
// 		.pipe(rename(function (path) {path.extname = path.extname.replace('jpeg', 'jpg')}))
// 		.pipe(gulp.dest('app/img/@1x'))
// });
// // Produce @2x images
// gulp.task('img-responsive-2x', async function() {
// 	return gulp.src('app/img/_src/**/*.{png,jpg,jpeg,webp,raw}')
// 		.pipe(newer('app/img/@2x'))
// 		.pipe(responsive({
// 			'**/*': { width: '100%', quality: quality }
// 		})).on('error', function (e) { console.log(e) })
// 		.pipe(rename(function (path) {path.extname = path.extname.replace('jpeg', 'jpg')}))
// 		.pipe(gulp.dest('app/img/@2x'))
// });
// gulp.task('img', gulp.series('img-responsive-1x', 'img-responsive-2x', bsReload));

// // Clean @*x IMG's
// gulp.task('cleanimg', function() {
// 	return del(['app/img/@*'], { force: true })
// });

// Code & Reload
gulp.task('code', function() {
	return gulp.src('./**/*.html')
	.pipe(browserSync.reload({ stream: true }))
});

// Deploy
gulp.task('rsync', function() {
	return gulp.src('app/')
	.pipe(rsync({
		root: 'app/',
		hostname: 'username@yousite.com',
		destination: 'yousite/public_html/',
		// include: ['*.htaccess'], // Included files
		exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excluded files
		recursive: true,
		archive: true,
		silent: false,
		compress: true
	}))
});

gulp.task('watch', function() {
	gulp.watch('./sass/**/*.sass', gulp.parallel('styles'));
	gulp.watch(['libs/**/*.js', './js/_custom.js', './js/_form.js'], gulp.parallel('scripts'));
	gulp.watch('./*.html', gulp.parallel('code'));
	gulp.watch('./js/preloader.js', gulp.parallel('preloader-script'));
	// gulp.watch('app/components/**/*.html', gulp.parallel('fileinclude-index', 'fileinclude-contacts', 'fileinclude-works'));
	// gulp.watch('app/img/_src/**/*', gulp.parallel('img'));
	// gulp.watch('app/*.php', gulp.parallel('php'));
	// gulp.watch('app/sass/preloader.sass', gulp.parallel('preloader-styles'));
  // gulp.watch('app/js/preloader.js', gulp.parallel('preloader-script'));
  // gulp.watch(['app/sass/**/*.sass', 'libs/**/*.js', 'libs/**/*.js', 'app/js/_custom.js', 'app/js/_form.js', 'app/*.html', 'app/components/**/*.html'], function(){
  //   server.notify.applay(server, [file]);
  // })
});

gulp.task('default', gulp.parallel('styles', 'scripts', 'preloader-script', 'browser-sync', 'watch'));
