const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require('fs');
const bodyParser = require('body-parser');
const Email = require('email-templates');
const nodemailer = require('nodemailer');
const multer = require('multer');

const upload = multer({dest: 'uploads/'});
const app = express();
// var jsonParser = bodyParser.json();

// var urlencodedParser = bodyParser.urlencoded({extended: false});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('*/css', express.static('public/css'));
app.use('*/js', express.static('public/js'));
app.use('*/img', express.static('public/img'));
app.use('*/fonts', express.static('public/fonts'));

function insyteRoute() {
	app.get('/i?(index)?(.html)?/insyte/', function (req, res) {
		res.sendFile('public/insyte/index.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/index.html', function (req, res) {
		res.sendFile('public/insyte/index.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/education.html', function (req, res) {
		console.log(1);
		res.sendFile('public/insyte/education.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/aboutus.html', function (req, res) {
		res.sendFile('public/insyte/aboutus.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/data-analysis.html', function (req, res) {
		res.sendFile('public/insyte/data-analysis.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/data-visualization.html', function (req, res) {
		res.sendFile('public/insyte/data-visualization.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/DR-SERVICE-Demand-Management.html', function (req, res) {
		res.sendFile('public/insyte/DR-SERVICE-Demand-Management.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/DR-SERVICE-plan.html', function (req, res) {
		res.sendFile('public/insyte/DR-SERVICE-plan.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/first-steps.html', function (req, res) {
		res.sendFile('public/insyte/first-steps.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/partners.html', function (req, res) {
		res.sendFile('public/insyte/partners.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/smart-energy.html', function (req, res) {
		res.sendFile('public/insyte/smart-energy.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/smart-farming.html', function (req, res) {
		res.sendFile('public/insyte/smart-farming.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/smart-metering.html', function (req, res) {
		res.sendFile('public/insyte/smart-metering.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/solutions.html', function (req, res) {
		res.sendFile('public/insyte/solutions.html', {root: __dirname});
	});

	app.get('/i?(index)?(.html)?/usecase.html', function (req, res) {
		res.sendFile('public/insyte/usecase.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Demand-Response-simulation.html', function (req, res) {
		res.sendFile('public/insyte/Demand-Response-simulation.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Basic-statistics.html', function (req, res) {
		res.sendFile('public/insyte/Basic-statistics.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Statictics-in-dynamics.html', function (req, res) {
		res.sendFile('public/insyte/Statictics-in-dynamics.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Statistics-in-static.html', function (req, res) {
		res.sendFile('public/insyte/Statistics-in-static.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Energy-balance.html', function (req, res) {
		res.sendFile('public/insyte/Energy-balance.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Energy-analysis.html', function (req, res) {
		res.sendFile('public/insyte/Energy-analysis.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Load-peak-forecasting.html', function (req, res) {
		res.sendFile('public/insyte/Load-peak-forecasting.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Statictics-for-the-fuelstation.html', function (req, res) {
		res.sendFile('public/insyte/Statictics-for-the-fuelstation.html', {root: __dirname});
  });
  
  app.get('/i?(index)?(.html)?/Benchmarking.html', function (req, res) {
		res.sendFile('public/insyte/Benchmarking.html', {root: __dirname});
	});
}

insyteRoute();


//Routes
app.get('/i?(index)?(.html)?', function (req, res) {
	res.render('index', {title: 'NA - Обо мне'});
});

// app.get('/b?(log)?(.html)?/developing-site', function (req, res) {
// 	res.render('blog', {title: 'NA - блог'});
// });

app.get('/c(ontacts)?(.html)?', function (req, res) {
	res.render('contacts', {title: 'NA - Контакты'});
});

app.get('/w(orks)?(.html)?', function (req, res) {
	var content = fs.readFileSync('./data/works-design.json', 'utf8');
	var works = JSON.parse(content);
	res.render('works', {title: 'NA - Мои работы', works: works});
});

app.get('/w(orks)?(.html)?/design/:id', function (req, res) {
	console.log('ID:', req.params.id);
	var content = fs.readFileSync('./data/works-design.json', 'utf8');
	var works = JSON.parse(content);
	var work;
	var nextWork;
	var nextWorkId;
	var prevWork;
	var prevWorkId;

	for (let i = 0; i < works.length; i++) {
		if (works[i].id === +req.params.id) {
			work = works[i];
			if (+req.params.id <= works.length - 1) {
				nextWork = works[i + 1].siteName;
				nextWorkId = works[i + 1].id;
				console.log('nextWork');
			}
			if (+req.params.id > 1) {
				prevWork = works[i - 1].siteName;
				prevWorkId = works[i - 1].id;
				console.log('prevWork');
			}
		}
	}
	res.render('works', {title: 'NA - Мои работы', id: req.params.id, work: work, nextWork: nextWork, nextWorkId: nextWorkId, prevWork: prevWork, prevWorkId: prevWorkId});
});

app.post('/i?(index)?(.html)?/design-order', function (req, res) {
	if (!req.body) return res.sendStatus(400);
	res.render('partials/forms/_design-order.pug');
});

app.post('/i?(index)?(.html)?/developing-order', function (req, res) {
	if (!req.body) return res.sendStatus(400);
	res.render('partials/forms/_developing-order.pug');
});

app.post('/i?(index)?(.html)?/order-send', function (req, res) {
	if (!req.body) return res.sendStatus(400);

	var fileForMessage;
	res.render('form-success', {formData: req.body, designBriefFormRequest: true}, function (err, html) {
		fileForMessage = html;
	});

	async function main() {
		// Generate test SMTP service account from ethereal.email
		// Only needed if you don't have a real mail account for testing
		let testAccount = await nodemailer.createTestAccount();

		// create reusable transporter object using the default SMTP transport
		let transporter = nodemailer.createTransport({
			host: 'smtp.ethereal.email',
			port: 587,
			secure: false, // true for 465, false for other ports
			auth: {
				user: 'bette64@ethereal.email',
				pass: 'WWVSQMpCCynCfHqzbM'
			}
		});

		// send mail with defined transport object
		let info = await transporter.sendMail({
			from: req.body.email, // sender address
			to: 'azfrontendsup@gmail.com', // list of receivers
			subject: 'Brief for Design Site', // Subject line
			html: fileForMessage // plain html body
		});
		// console.log('Message sent: %s', info.messageId);
		// // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
		//
		// // Preview only available when sending through an Ethereal account
		// console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
		// // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
	}

	main().catch(console.error);
	res.render('form-success', {formData: req.body, designBriefFormRequest: true});
});

app.post('/i?(index)?(.html)?/popup-brief-design-site', function (req, res) {
	if (!req.body) return res.sendStatus(400);
	res.render('partials/forms/popup-design-brief');
});

app.post('/i?(index)?(.html)?/finished-design-brief', upload.array('client-logotype', 3), function (req, res) {
	if (!req.body) return res.sendStatus(400);

	var fileForMessage;
	res.render('form-success', {formData: req.body, designBriefFormRequest: true}, function (err, html) {
		fileForMessage = html;
	});

	async function main() {
		// Generate test SMTP service account from ethereal.email
		// Only needed if you don't have a real mail account for testing
		let testAccount = await nodemailer.createTestAccount();

		// create reusable transporter object using the default SMTP transport
		let transporter = nodemailer.createTransport({
			host: 'smtp.ethereal.email',
			port: 587,
			secure: false, // true for 465, false for other ports
			auth: {
				user: 'bette64@ethereal.email',
				pass: 'WWVSQMpCCynCfHqzbM'
			}
		});

		// send mail with defined transport object
		let info = await transporter.sendMail({
			from: req.body.email, // sender address
			to: 'azfrontendsup@gmail.com', // list of receivers
			subject: 'Brief for Design Site', // Subject line
			html: fileForMessage // plain html body
		});
		// console.log('Message sent: %s', info.messageId);
		// // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
		//
		// // Preview only available when sending through an Ethereal account
		// console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
		// // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
	}

	main().catch(console.error);
	res.render('form-success', {formData: req.body, designBriefFormRequest: true});
});

// app.post('/i?(index)?(.html)?/finished-design-brief/image', ), [
//       // validation ...
//     ], (req, res) => {
//       // error handling ...
//       if (req.file) {
//         console.log('Uploaded: ', req.file)
//         // Homework: Upload file to S3
//       }
//       req.flash('success', 'Thanks for the message! I’ll be in touch')
//       res.redirect('/')
// });

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
